import os
from setuptools import setup

def requirements():
    """Returns the requirements for this package.

    If the `json` module can not be imported, then `simplejson` is needed.
    `Simplejson` was added to Python 2.6 as `json`.
    """
    requires = ['boto']
    try:
        import json
    except ImportError:
        requires.append('simplejson')
    return requires

def read(fname):
    """Utility function to read the README file."""
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "fary",
    version = "0.1",
    author = "David Losada",
    author_email = "david@tuxpiper.com",
    description = ("Syncs directories to buckets on S3."),
    license = "Apache Software License 2.0",
    keywords = "S3 Amazon sync",
    url = "https://bitbucket.org/tuxpiper/fary",
    packages = ['fary'],
    long_description = read('README.rst'),
    install_requires = ['boto'],
) 
