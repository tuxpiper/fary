"""
Recursively sync a file directory to S3
"""
from collections import defaultdict
from datetime import datetime
import hashlib
from optparse import OptionParser
import os
from os.path import join
import re

from boto.s3.connection import S3Connection
from boto.s3.key import Key
from boto.s3.key import compute_md5

class Synchronizer(object):
    """Syncronizes a directory to a bucket on S3."""

    def __init__(self, connection, local_folder, s3_location):
        """
        Constructor for the Synchronizer objects.
            * connection - boto S3 connection to be used
            * local_folder - local folder to be synchronized (origin)
            * s3_location - s3 location to be synchronized (target),
                format spec: s3://<bucket>/[path/within/bucket]
        """
        s3_loc_re = re.compile("s3://(?P<bucket>[\w\-\.]+)(/(?P<path>\S+))?")
        self.conn = connection
        self.local_folder = local_folder
        # Parse s3 location for bucket and path
        match = s3_loc_re.match(s3_location);
        if match is None:
            raise RuntimeError("Invalid S3 location provided")
        self.s3_bucket = match.groupdict()['bucket']
        self.s3_path = match.groupdict()['path']
        if self.s3_path is None:
            self.s3_path = ""

    def _get_local_files(self, path):
        """Returns a dictionary of all the files under a path."""
        if not path:
            raise ValueError("No path specified")
        files = defaultdict(lambda: None)
        path_len = len(path) + 1
        for root, dirs, filenames in os.walk(path):
            for name in filenames:
                full_path = join(root, name)
                files[full_path[path_len:]] = compute_md5_from_path(full_path) 
        return files

    def _get_s3_files(self, bucket, path):
        if not bucket:
            raise ValueError("No bucket specified")

        files = defaultdict(lambda: None)
        for f in bucket.list(prefix=path):
            if f.name.endswith('$folder$'):
                continue
            files[f.name] = f
        return files

    def sync(self):
        """Syncs local directory with an S3 bucket.
     
        Currently does not delete files from S3 that are not in the local directory.
        """
        bucket = self.conn.get_bucket(self.s3_bucket)
        local_files = self._get_local_files(self.local_folder)
        s3_files = self._get_s3_files(bucket, self.s3_path)
        for filename, hasha in local_files.iteritems():
            s3_filename = os.path.join(self.s3_path, filename)
            s3_key = s3_files[s3_filename]
            if s3_key is None:
                s3_key = Key(bucket)
                s3_key.key = s3_filename
                s3_key.etag = '"!"'
            if s3_key.etag[1:-1] != hasha[0]:
                s3_key.set_contents_from_filename(os.path.join(self.local_folder, filename), md5=hasha)

def compute_md5_from_path(path):
    fp = open(path)
    result = compute_md5(fp)
    fp.close()
    return result

